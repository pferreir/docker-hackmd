#!/bin/sh

oc create -f hackmd-imagestream.yaml
oc create -f hackmd-buildconfig.yaml
oc create -f hackmd-deploymentconfig.yaml
oc create -f hackmd-service.yaml
oc create -f hackmd-volumeclaim.yaml
